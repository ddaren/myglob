#ifndef LIBUTF8_H__
#define LIBUTF8_H__
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int utf8_encode(unsigned char *buf, uint32_t c);
uint32_t utf8_decode(char *p, char **newp);
#endif

#ifdef LIBUTF8_IMPLEMENTATION
/* implementation */
/* Encode a character in utf8 */
int utf8_encode(unsigned char *buf, uint32_t c) {
  if (c <= 0x7F) {
    buf[0] = c & 0b11111111;
    return 1;
  } else if (c <= 0x7FF) {
    buf[0] = 0b11000000 | (c >> 6 & 0b11111111);
    buf[1] = 0b10000000 | (c & 0b00111111);
    return 2;
  } else if (c <= 0xFFFF) {
    buf[0] = 0b11100000 | (c >> 12 & 0b11111111);
    buf[1] = 0b10000000 | (c >> 6 & 0b00111111);
    buf[2] = 0b10000000 | (c & 0b00111111);
    return 3;
  } else if (c <= 0x10FFFF) {
    buf[0] = 0b11110000 | (c >> 18 & 0b11111111);
    buf[1] = 0b10000000 | (c >> 12 & 0b00111111);
    buf[2] = 0b10000000 | (c >> 6 & 0b00111111);
    buf[3] = 0b10000000 | (c & 0b00111111);
    return 4;
  }

  return 0;
}

/* Decode an utf8 sequence to codepoint */
/* return codepoint and new position of utf8 sequence */
uint32_t utf8_decode(char *p, char **newp) {
  if (*p <= 0b01111111) {
    *newp = p + 1;
    return *p;
  }

  int len = 0;
  uint32_t c;

  unsigned char byte1 = (unsigned char)*p;

  if (byte1 <= 0b11011111) {
    len = 2;
    c = 0b00011111 & byte1;
  } else if (byte1 <= 0b11101111) {
    len = 3;
    c = 0b00001111 & byte1;
  } else if (byte1 <= 0b11110111) {
    len = 4;
    c = 0b00000111 & byte1;
  } else {
    assert(0 && "invalid utf8 sequence");
  }

  for (int i = 1; i < len; i++) {
    unsigned char bytei = p[i];
    if (bytei >> 6 != 0b010) assert(0 && "invalid utf8 sequence");
    c = c << 6 | (bytei & 0b00111111);
  }

  *newp = p + len;
  return c;
}

bool utf8_is_legal(char *p) {
  if (*p <= 0b01111111) {
    return true;
  }

  int len = 0;

  unsigned char byte1 = (unsigned char)*p;

  if (byte1 <= 0b11011111) {
    len = 2;
  } else if (byte1 <= 0b11101111) {
    len = 3;
  } else if (byte1 <= 0b11110111) {
    len = 4;
  }

  if (len < 1) {
    fprintf(stderr, "%s is invalid utf8 sequence.\n", p);
    return false;
  }

  for (int i = 1; i < len; i++) {
    unsigned char bytei = p[i];
    if (bytei >> 6 != 0b010) {
      fprintf(stderr, "%s is invalid utf8 sequence.\n", p);
      return false;
    }
  }

  return true;
}
#endif
