#ifndef MYGLOB_H__
#define MYGLOB_H__
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define LIBUTF8_IMPLEMENTATION
#include "libutf8.h"

typedef enum {
  RESULT_MATCHED = 0,
  RESULT_UNMATCHED,
  RESULT_SYNTAX_ERROR,
} Result;

char *result_display(Result res);
Result glob_utf8(char *pattern, char *text);

#endif

#ifdef MYGLOB_IMPLEMENTATION

char *result_display(Result res) {
  switch (res) {
    case RESULT_MATCHED:
      return "RESULT_MATCHED";
    case RESULT_UNMATCHED:
      return "RESULT_UNMATCHED";
    case RESULT_SYNTAX_ERROR:
      return "RESULT_SYNTAX_ERROR";
  }

  assert(0 && "unknown RESULT");
}

Result glob_utf8(char *pattern, char *text) {
  static char *pat_newp[1];
  static char *txt_newp[1];

  while (*pattern != '\0' && *text != '\0') {
    switch (*pattern) {
        // match any single char
      case '?': {
        pattern++;
        utf8_decode(text, txt_newp);
        text = *txt_newp;
        break;  // escape from switch-clause
      }

        // match any
      case '*': {
        Result res = glob_utf8(pattern + 1, text);
        if (res == RESULT_UNMATCHED) {
          /* text++; */
          utf8_decode(text, txt_newp);
          text = *txt_newp;
          break;
        } else
          return res;
      }

      // match anyone in [abc] or [a-z] or [0-9] or [A-Z].
      case '[': {
        bool matched = false;
        bool negate = false;

        pattern++;  // skip [
        if (*pattern == '\0') return RESULT_SYNTAX_ERROR;

        if (*pattern == '!') {
          negate = true;
          pattern++;  // skip !
          if (*pattern == '\0') return RESULT_SYNTAX_ERROR;
        }

        /* matched |= *pattern == *text; */
        uint32_t prev = utf8_decode(pattern, pat_newp);
        matched |= prev == utf8_decode(text, txt_newp);
        /* char *prev = pattern; */
        pattern = *pat_newp;

        while (*pattern != ']' && *pattern != '\0') {
          switch (*pattern) {
            case '-': {
              uint32_t dash = utf8_decode(pattern, pat_newp);
              pattern = *pat_newp;  // skip -
              switch (*pattern) {
                case ']':
                  matched |= dash == utf8_decode(text, txt_newp);
                  break;
                case '\0':
                  return RESULT_SYNTAX_ERROR;
                default: {
                  uint32_t tc = utf8_decode(text, txt_newp);
                  matched |= prev <= tc && tc <= utf8_decode(pattern, pat_newp);
                  pattern = *pat_newp;
                }
              }
            } break;
            default: {
              matched |=
                  utf8_decode(pattern, pat_newp) == utf8_decode(text, txt_newp);

              pattern = *pat_newp;
            }
          }
        }

        if (*pattern == '\0' || *pattern != ']') return RESULT_SYNTAX_ERROR;
        if (negate) matched = !matched;

        if (!matched) return RESULT_UNMATCHED;

        pattern++;  // skip ]
        text = *txt_newp;
      } break;

        // 转义符
      case '\\':
        pattern++;  // skip /
        if (*pattern == '\0') return RESULT_SYNTAX_ERROR;
        // fallthrough

      // compare char
      default: {
        if (utf8_decode(pattern, pat_newp) == utf8_decode(text, txt_newp)) {
          pattern = *pat_newp;
          text = *txt_newp;
        } else {
          return RESULT_UNMATCHED;
        }
      }
    }
  }

  if (*text == '\0') {
    while (*pattern == '*') pattern++;
    if (*pattern == '\0')
      return RESULT_MATCHED;
    else
      return RESULT_UNMATCHED;
  }

  return RESULT_UNMATCHED;
}

#endif
