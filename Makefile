CC=gcc
CFLAGS=-std=c99 -Wall -g
SRCS=$(wildcard *.c)
OBJS=$(SRCS:.c=.o)

glob:$(OBJS)
	$(CC) $(CFLAGS) -o $@ $?

test:
	./glob

clean:
	rm *.o glob
