# what is it?

Yet another glob.h, with supporting for utf8.

# how to use?

```c
#include <stdio.h>

#define MYGLOB_IMPLEMENTATION
#include "myglob.h"

int main(void){
  if (!glob_utf8("你?世界", "你好世界")){
    printf("OK.\n");
  } else {
    printf("Failed.\n");
  }
  
  return 0;
}
```

# testing
``make && make test``
